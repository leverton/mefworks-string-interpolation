mef\StringInterpolation - Embed contextual data into a string
=============================================================

[![Total Downloads](https://poser.pugx.org/mefworks/string-interpolation/downloads.png)](https://packagist.org/packages/mefworks/string-interpolation)
[![Latest Stable Version](https://poser.pugx.org/mefworks/string-interpolation/v/stable.png)](https://packagist.org/packages/mefworks/string-interpolation)

mef\StringInterpolation defines the interface `StringInterpolationInterface`
that describes a method to combine an array of arbitrary data with a string,
much like the PHP methods `vsprintf` and `strtr`.

The core implementation of `PlaceholderInterpolator` replaces `{0}` and
`{key}` with the corresponding values from the context array, but other
implementations of the interface are free to use any syntax.

It is recommended that interpolators make use of
[mef\Stringifier](https://bitbucket.org/leverton/mefworks-stringify) to convert
non-string values from the context array into a string.

Examples
--------

*PlaceholderInterpolator*


```php
<?php
$stringifier = new mef\Stringifier\Stringifier;
$interpolator = new mef\StringInterpolation\PlaceholderInterpolator($stringifier);

echo $interpolator('Hello, {name}!', ['name' => 'Matthew']), PHP_EOL;

echo $interpolator->getInterpolatedString('Index based 0: {0}, 1: {1}', ['one', 'two']), PHP_EOL;

```

Sample Output:

	Hello, Matthew!
	Index based 0: one, 1: two

The `Stringifier` is what is responsible for casting the value in the array to
a string. In this case, all values were already strings, so it is a somewhat
irrelevant detail. Note that the functor call (first echo) is from the
`AbstractStringInterpolator`, and is not guaranteed to exist in all
interpolators since the interface does not require it.

More complete examples are available in the `examples` directory.

Overview
--------
The `mef\StringInterpolation\StringInterpolatorInterface` describes two
methods:

```php
<?php namespace mef\StringInterpolation;

interface StringInterpolatorInterface
{
	/**
	 * Given a string and an array of context, return a new Interpolation
	 * record, from which the interpolated string and unused context can be
	 * retrieved.
	 *
	 * The implementation details are left undefined.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return mef\StringInterpolation\InterpolationInterface
	 */
	public function interpolate($string, $context);

	/**
	 * Given a string and an array of context, return a new string with that
	 * context embedded in it.
	 *
	 * This MUST always give the same results as:
	 *
	 * ->interpolate($string, $context)->getString()
	 *
	 * This variation should be optimized when possible, as there's no need to
	 * create an Interpolation object or track unused context.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return string
	 */
	public function getInterpolatedString($string, $context);
}
```

Both methods perform the same underlying operation.
`getInterpolatedString(...)` returns the same thing as
`interpolate(...)->getString()` but is slightly faster since no `Interpolation`
object is created and the context does not need to be updated.

The interpolator object is defined by this interface:

```php
<?php namespace mef\StringInterpolation;

interface InterpolationInterface
{
	/**
	 * The interpolated string.
	 *
	 * @return string
	 */
	public function getString();

	/**
	 * The used context.
	 *
	 * @return array
	 */
	public function getUsedContext();
}
```

Contexts
--------

The context supplied to an interplator can either be a native PHP array, or an
object that implements `ContextInterface`.

* `ArrayContext` - Wraps an array into an object that implements the
`ContextInterface`.

Interpolators
-------------

* `ExpressionInterpolator` - Treats `{key}` and `{0}` as placeholders, and
uses a string interpolator to replace them with the corresponding values from
the context array. Values can be filtered through expressions that mutate the
output. e.g., `{key|uppercase}`
* `IndirectInterpolator` - A decorator that looks up a string format from an
ArrayAccess object before passing it to the internal interpolator. Useful for
creating a resource bundle.
* `PlaceholderInterpolator` - Similar to `ExpressionInterpolator`, but it does
not support an expressive syntax.
* `PrintFInterpolator` - Uses a `printf` style of interpolation.

License
=======

Copyright (C) 2014 Matthew Leverton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
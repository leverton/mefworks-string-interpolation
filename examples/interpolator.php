<?php

require __DIR__ . '/../vendor/autoload.php';

$stringifier = new mef\Stringifier\Stringifier;
$interpolator = new mef\StringInterpolation\PlaceholderInterpolator($stringifier);

// There are three ways to use any interpolator that extends the AbstractStringInterpolator:

// 1: as a functor (alias for getInterpolatedString)
echo $interpolator('Hello, {name}!', ['name' => 'Matthew']), PHP_EOL;

// 2: getInterpolatedString
echo $interpolator->getInterpolatedString('Index based 0: {0}, 1: {1}', ['one', 'two']), PHP_EOL;

// 3: interpolate -- to get more context
$interpolation = $interpolator->interpolate('Missing: {missing}, Valid: {valid}', [
	'valid' => 'VALID',
	'unused' => 'UNUSED'
]);

echo $interpolation->getString(), PHP_EOL;

echo 'Used values: ';
var_dump($interpolation->getUsedContext());
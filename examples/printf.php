<?php

require __DIR__ . '/../vendor/autoload.php';

$stringifier = new mef\Stringifier\Stringifier;

// The PrintFInterpolator is a wrapper over vsprintf.

$interpolator = new mef\StringInterpolation\PrintFInterpolator($stringifier);

echo $interpolator("Hello, %s %s!", ["World"]), PHP_EOL;

$context = [2, "not", "used"];

$interpolation = $interpolator->interpolate("Extra Parameters: %d", $context);

echo $interpolation->getString(), "\n";
var_dump( array_diff_key($context, $interpolation->getUsedContext()) );

// All arrays are cast to indexed arrays and read from left to right.
echo $interpolator("Hello, %s!", ['Object' => 'World']), PHP_EOL;
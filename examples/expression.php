<?php

require __DIR__ . '/../vendor/autoload.php';

$foo = new mef\StringInterpolation\ExpressionInterpolator;
$foo->extendFilters([
	'upper' => function($string) { return strtoupper($string); },
	'lower' => function($string) { return strtolower($string); }
]);


echo $foo->getInterpolatedString('Hello, {0}!', ['World']), PHP_EOL;
echo $foo->getInterpolatedString('Hello, {0:upper}!', ['World']), PHP_EOL;
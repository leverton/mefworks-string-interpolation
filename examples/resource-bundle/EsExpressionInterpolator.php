<?php
/**
 * The Spanish expression interpolator.
 *
 * Extends the default (English) implementation.
 */
class EsExpressionInterpolator extends EnExpressionInterpolator
{
	public function __construct($locale)
	{
		parent::__construct($locale);

		$this->filters = array_merge($this->filters, [
			// Spanish specific filters would go here
		]);
	}
}
<?php

/**
 * The default (English) expression interpolator for the resource bundle.
 */
class EnExpressionInterpolator extends mef\StringInterpolation\ExpressionInterpolator
{
	public function __construct($locale = 'en-US')
	{
		// Set up a currency formatter to be used by the filters
		$currencyFormatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);

		// Set up the filters that the language file can access
		$this->filters = [
			'uppercase' => 'strtoupper',
			'lowercase' => 'strtolower',
			'substring' => 'substr',
			'pp' => function ($value) {
				return ($value === 'm') ? 'his' : 'her';
			},
			'if' => function ($value, $target, $true, $false) {
				return ($value == $target) ? $true : $false;
			},
			'currency' => function ($value, $type = 'USD') use ($currencyFormatter) {
				return $currencyFormatter->formatCurrency($value, $type);
			},

			// This one is just a proof of concept to show that even without an
			// advanced syntax (which may come later), it is still possible to
			// do "complex" things, such as chaining. e.g., To lowercase the
			// first three letters.
			//
			// {0|compose:substr:0:3::lowercase}
			//
			// (This type logic may belong in the view instead of the language,
			// but sometimes there are language specific functions that need to
			// be called.)
			//

			/* Requires PHP 5.6, so commented out.

			'compose' => function ($value, ...$params) {
				while ($params)
				{
					$method = array_shift($params);
					$args = [];

					while ($params && ($param = array_shift($params)) !== '')
					{
						$args[] = $param;
					}

					$value = $this->filters[$method]($value, ...$args);
				}

				return $value;
			}
			*/
		];
	}
}
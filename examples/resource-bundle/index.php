<?php

use mef\StringInterpolation\IndirectInterpolator;

require_once __DIR__ . '/../../vendor/autoload.php';

require_once __DIR__ . '/EnExpressionInterpolator.php';
require_once __DIR__ . '/EsExpressionInterpolator.php';

/**
 * The interpolator by itself is somewhat cumbersome to use as a resource
 * bundle, but creating a view friendly method is easy.
 *
 * @param  mef\StringInterpolation\StringInterpolatorInterface $bundle
 *
 * @return callable
 */
function createViewHelper($bundle)
{
	return function() use ($bundle)
	{
		$params = func_get_args();
		$key = array_shift($params);

		return $bundle->getInterpolatedString($key, $params);
	};
}

/**
 * Build the resource bundle.
 *
 * The application needs to do something like this to set up the resource
 * bundle. This example just loads from a language specific INI file and a
 * default fall back one. If a string isn't found in the language file, the
 * default one will be used instead.
 *
 * For production purposes, this is something that could be optimized into a
 * a "compile" stage to avoid unnecessary runtime overhead.
 *
 * @param  string $locale
 *
 * @return mef\StringInterpolator\IndirectInterpolator
 */
function getResourceBundle($locale = 'en-US')
{
	list($language, $country) = explode('-', $locale);

	// The ExpressionInterpolator is the most powerful one. It supports a
	// syntax like {arg|filter:param1:param2}. For the purposes of this
	// example, we've created two extensions of that for English and Spanish.
	// Those classes define custom functions, such as how to handle plural
	// formats, etc. Here, the Spanish version extends from the English
	// version since sometimes the logic is the same for both.
	//

	// 1. Create expression interpolator based off the locale's language
	$reflector = new ReflectionClass($language . 'ExpressionInterpolator');
	$expressionInterpolator = $reflector->newInstance($locale);

	// 2. Create the data source by combining the specified langauge with the
	// default language file.
	$default = parse_ini_file(__DIR__ . '/default.ini');
	$language = parse_ini_file(__DIR__ . '/' . $locale . '.ini');
	$dataSource = new ArrayObject(array_merge($default, $language));

	// 3. Create the "resource bundle" interpolator by combining 1 & 2.
	return new IndirectInterpolator($dataSource, $expressionInterpolator);
}

$english = getResourceBundle('en-US');
$spanish = getResourceBundle('es-ES');

// Without a helper function, it would be used like this:

echo $english('HELLO', []), PHP_EOL;
echo $spanish('HELLO', []), PHP_EOL;

// With a helper:

$_e = createViewHelper($english);
$_s = createViewHelper($spanish);

// (Normally the view would only need a single helper method, perhaps called $_)

echo $_e('HELLO'), PHP_EOL;
echo $_s('HELLO'), PHP_EOL;

// Passing arguments:

echo $_e('EAT', 'Matthew', 'm'), PHP_EOL;
echo $_s('EAT', 'Matthew', 'm'), PHP_EOL;

// Locale-specific data. Note that the Spanish INI does not have the USD
// identifier since it is the same as the default implementation.

echo $_e('USD', '12.34'), PHP_EOL;
echo $_s('USD', '12.34'), PHP_EOL;
<?php namespace mef\StringInterpolation;

use ArrayObject;
use InvalidArgumentException;

abstract class Functions
{
	static public function castContext($object)
	{
		if (is_array($object))
		{
			return new ArrayContext($object);
		}
		else if ($object instanceof ContextInterface)
		{
			return $object;
		}
		else
		{
			throw new InvalidArgumentException("Argument must be an array or an implementation of ContextInterface");
		}
	}
}
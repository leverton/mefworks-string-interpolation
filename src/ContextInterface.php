<?php namespace mef\StringInterpolation;

interface ContextInterface
{
	public function getValue($key);

	public function iterate();
}
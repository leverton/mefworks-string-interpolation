<?php namespace mef\StringInterpolation;

abstract class AbstractStringInterpolator implements StringInterpolatorInterface
{

	/**
	 * The sub class must implement this method.
	 *
	 * The string and context are passed by reference. The string must always
	 * be updated with the final result. The context must be modified IF
	 * $updateContext is true. It is okay to modify the context when
	 * $updateContext is false, but it will be ignored.
	 *
	 * @param  string  &$string        The string that needs to be embedded
	 * @param  array   &$context       The context array.
	 * @param  boolean $updateContext  If true, you must update the $context
	 */
	abstract protected function doInterpolation(&$string, ContextInterface &$context, $updateContext);

	/**
	 * Interpolate the string with the given context array.
	 *
	 * See the implementing class documentation for details on how the
	 * interpolation takes place.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array  $context An array of contextual data
	 *
	 * @return \mef\StringInterpolation\Interpolation
	 */
	public function interpolate($string, $context)
	{
		$string = (string) $string;
		$context = Functions::castContext($context);

		$this->doInterpolation($string, $context, true);

		return new Interpolation($string, $context);
	}

	/**
	 * Given a string and an array of context, return a new string with that
	 * context embedded in it.
	 *
	 * This gives the same results as:
	 *
	 * ->interpolate($string, $context)->getString()
	 *
	 * but is more optimized, and should be used if you do not need the unused
	 * context information.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array  $context An array of contextual data
	 *
	 * @return string
	 */
	public function getInterpolatedString($string, $context)
	{
		$string = (string) $string;
		$context = Functions::castContext($context);

		$this->doInterpolation($string, $context, false);

		return $string;
	}

	/**
	 * An alias for $this->getInterpolatedString()
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array  $context An array of contextual data
	 *
	 * @return string
	 */
	public function __invoke($string, $context)
	{
		$string = (string) $string;
		$context = Functions::castContext($context);

		$this->doInterpolation($string, $context, false);

		return $string;
	}
}
<?php namespace mef\StringInterpolation;

interface InterpolationInterface
{
	/**
	 * The interpolated string.
	 *
	 * @return string
	 */
	public function getString();

	/**
	 * The used context.
	 *
	 * @return array
	 */
	public function getUsedContext();
}
<?php namespace mef\StringInterpolation;

interface StringInterpolatorAwareInterface
{
	/**
	 * @return  mef\StringInterpolation\StringInterpolatorInterface
	 */
	public function getStringInterpolator();

	/**
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 */
	public function setStringInterpolator(StringInterpolatorInterface $interpolator);
}
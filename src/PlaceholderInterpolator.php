<?php namespace mef\StringInterpolation;

use mef\Stringifier\StringifierAwareTrait;
use mef\Stringifier\StringifierAwareInterface;

/**
 * A string interpolator that replaces {placeholders} with information from
 * the context array.
 *
 * This uses a Stringifier to convert the context array into a string.
 *
 * The placeholders are curly braces {}. If an item in the array exists
 * with that same name (minus the braces), it is stringified via the
 * class stringifier.
 *
 * E.g., 'Hello, {person}!', ['person' => 'Matthew'] would result in the
 * following: 'Hello, Matthew!'.
 *
 * If a placeholder does not exist in the context array, then null is used
 * as a default value.
 */
class PlaceholderInterpolator extends AbstractStringInterpolator implements
	StringifierAwareInterface
{
	use StringifierAwareTrait;

	/**
	 * Constructor
	 *
	 * @param \mef\Stringifier\StringifierInterface $stringifier
	 */
	public function __construct(\mef\Stringifier\StringifierInterface $stringifier)
	{
		$this->stringifier = $stringifier;
	}

	protected function doInterpolation(&$string, ContextInterface &$context, $updateContext)
	{
		$usedContext = [];

		// If it contains no special characters, then we can skip all
		// processing.
		if (strpbrk($string, '\\{') !== false)
		{
			$stringLength = strlen($string);

			$output = ['tag' => '', 'message' => ''];
			$mode = 'message';

			for ($i = 0; $i < $stringLength; ++$i)
			{
				$c = $string[$i];

				if ($c === '\\' && $i < $stringLength - 1)
				{
					// an escaped character
					$d = $string[$i + 1];

					if ($d === '\\' || $d === '{' || $d === '}')
					{
						// recognized, gobble the backslash
						$output[$mode] .= $d;
						++$i;
					}
					else
					{
						// unknown, treat it as a backslash
						$output[$mode] .= '\\';
					}
				}
				else if ($c === '{' && $mode === 'message')
				{
					// starting a new tag
					$mode = 'tag';
					$output['tag'] = '';
				}
				else if ($c === '}' && $mode === 'tag')
				{
					// ending a tag
					$key = $output['tag'];

					$value = $context->getValue($key);

					if ($updateContext === true)
					{
						$usedContext[$key] = $value;
					}

					$mode = 'message';

					$output['message'] .= $this->stringifier->stringify($value);
					$output['tag'] = '';
				}
				else
				{
					$output[$mode] .= $c;
				}
			}

			$string = $output['message'];

			if ($output['tag'] !== '')
			{
				// a tag was left open
				$string .= '{' . $output['tag'];
			}
		}

		if ($updateContext === true)
		{
			$context = $usedContext;
		}
	}
}
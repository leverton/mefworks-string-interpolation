<?php namespace mef\StringInterpolation;

interface StringInterpolatorInterface
{
	/**
	 * Given a string and an array of context, return a new Interpolation
	 * record, from which the interpolated string and used context can be
	 * retrieved.
	 *
	 * The implementation details are left undefined.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return \mef\StringInterpolation\InterpolationInterface
	 */
	public function interpolate($string, $context);

	/**
	 * Given a string and an array of context, return a new string with that
	 * context embedded in it.
	 *
	 * This MUST always give the same results as:
	 *
	 * ->interpolate($string, $context)->getString()
	 *
	 * This variation should be optimized when possible, as there's no need to
	 * create an Interpolation object or track unused context.
	 *
	 * @param  string $string  The string to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return string
	 */
	public function getInterpolatedString($string, $context);
}
<?php namespace mef\StringInterpolation;

use LimitIterator;

use mef\Stringifier\StringifierAwareTrait;
use mef\Stringifier\StringifierAwareInterface;

/**
 * An interpolator powered by PHP's vsprintf.
 *
 * The context is always converted to an indexed array and read from left to
 * right, regardless of which keys are integers and strings.
 */
class PrintFInterpolator extends AbstractStringInterpolator
{
	/**
	 * Interpolate based on vsprintf.
	 *
	 * @param  string  &$string
	 * @param  array   &$context
	 * @param  boolean $updateContext
	 */
	protected function doInterpolation(&$string, ContextInterface &$context, $updateContext)
	{
		if (!preg_match_all('/(?:%+)/', $string, $matches))
		{
			// No placeholders - nothing to do
			if ($updateContext === true)
			{
				// ...except for updating the context if needed
				$context = [];
			}
		}
		else
		{
			// Check counts to see if we have the right amount.
			// Consecutive odd number of % signs means one placeholder
			// because %% is a literal percent sign. e.g., %f %%%f
			$expectedArgCount = array_sum(array_map(function($match) {
				return strlen($match) & 1;
			}, $matches[0]));

			$args = iterator_to_array(new LimitIterator($context->iterate(),
				0, $expectedArgCount));

			$actualArgCount = count($args);
			if ($actualArgCount < $expectedArgCount)
			{
				array_splice($args, $actualArgCount, 0,
					array_fill(0, $expectedArgCount - $actualArgCount, null));
			}

			// Do the interpolation
			$string = vsprintf($string, $args);

			if ($updateContext === true)
			{
				$context = $args;
			}
		}
	}
}
<?php namespace mef\StringInterpolation;

/**
 * A trait that satisfies the StringInterpolatorAwareInterface.
 */
trait StringInterpolatorAwareTrait
{
	protected $stringInterpolator;

	/**
	 * @return \mef\StringInterpolation\StringInterpolatorInterface
	 */
	final public function getStringInterpolator()
	{
		return $this->stringInterpolator;
	}

	/**
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 */
	final public function setStringInterpolator(StringInterpolatorInterface $interpolator)
	{
		$this->stringInterpolator = $interpolator;
	}
}
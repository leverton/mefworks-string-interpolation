<?php namespace mef\StringInterpolation;

use ArrayObject;

class ArrayContext extends ArrayObject implements ContextInterface
{
	public function getValue($key)
	{
		return $this->offsetExists($key) ? $this->offsetGet($key) : null;
	}

	public function iterate()
	{
		return $this->getIterator();
	}
}
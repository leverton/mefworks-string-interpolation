<?php namespace mef\StringInterpolation;

use InvalidArgumentException;

use mef\Stringifier\StringifierAwareTrait;
use mef\Stringifier\StringifierAwareInterface;

/**
 * Uses indexed placeholders with expressions.
 *
 * An expression looks like {n|filter:param1:param2}, where:
 *
 *   n is an integer
 *   filter is a name of a known filter
 *   param1, param2, ... are parameters passed to the filter
 *
 * The filter is a function that looks like:
 *
 *   function myFilter($value, $param1, $param2, ...)
 *
 * $value is the associated value from the $context array
 * $param1, $param2, ... are the hardcoded values from the expression
 */
class ExpressionInterpolator extends AbstractStringInterpolator implements
	StringifierAwareInterface
{
	use StringifierAwareTrait;

	/**
	 * An array of filter callbacks
	 *
	 * @var array
	 */
	protected $filters = [];

	/**
	 * Constructor
	 *
	 * @param \mef\Stringifier\StringifierInterface $stringifier
	 */
	public function __construct(\mef\Stringifier\StringifierInterface $stringifier = null)
	{
		$this->stringifier = $stringifier;
	}

	/**
	 * The string and context are passed by reference. The string must always
	 * be updated with the final result. The context must be modified IF
	 * $updateContext is true. It is okay to modify the context when
	 * $updateContext is false, but it will be ignored.
	 *
	 * @param  string  &$string        The string that needs to be embedded
	 * @param  array   &$context       The context array.
	 * @param  boolean $updateContext  If true, you must update the $context
	 */
	protected function doInterpolation(&$string, ContextInterface &$context, $updateContext)
	{
		$usedContext = [];

		if (preg_match_all('/{([^}]+)}/', $string, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER))
		{
			foreach (array_reverse($matches) as $match)
			{
				$offset = $match[0][1];
				$length = strlen($match[0][0]);

				$parts = explode('|', $match[1][0]);

				$key = array_shift($parts);

				$replacement = $context->getValue($key);

				if ($this->stringifier !== null)
				{
					$replacement = $this->stringifier->stringify($replacement);
				}

				if ($updateContext === true)
				{
					$usedContext[$key] = $replacement;
				}

				if (count($parts))
				{
					$parts = explode(':', $parts[0]);

					$filter = array_shift($parts);
					if (isset($this->filters[$filter]) === false)
					{
						throw new InvalidArgumentException("Unknown filter: $filter");
					}

					// PHP 5.6: $replacement = $this->filters[$filter]($replacement, ...$parts);
					array_unshift($parts, $replacement);
					$replacement = call_user_func_array($this->filters[$filter], $parts);
				}

				$string = substr($string, 0, $offset) . $replacement . substr($string, $offset + $length);
			}
		}

		if ($updateContext === true)
		{
			$context = $usedContext;
		}
	}

	/**
	 * Set the filter for the given id.
	 *
	 * @param string   $id
	 * @param callable $filter
	 */
	public function setFilter($id, callable $filter)
	{
		$this->filters[$id] = $filter;
	}

	/**
	 * Replace all filters with the given array.
	 *
	 * @param array $filters An array (map) of callable filters
	 */
	public function setFilters(array $filters)
	{
		foreach ($filters as $filter)
		{
			if (!is_callable($filter))
			{
				throw new InvalidArgumentException("Filters must be callable");
			}
		}

		$this->filters = $filters;
	}

	/**
	 * Extend the existing list of filters with the new list.
	 *
	 * Any filter that already exists for a given id will be replaced by the
	 * filter from the new list.
	 *
	 * @param array $filters An array (map) of callable filters
	 */
	public function extendFilters(array $filters)
	{
		foreach ($filters as $key => $filter)
		{
			if (!is_callable($filter))
			{
				throw new InvalidArgumentException("Filters must be callable");
			}

			$this->filters[$key] = $filter;
		}
	}

	/**
	 * Return the list of filters.
	 *
	 * @return array
	 */
	public function getFilter($id)
	{
		if (!isset($this->filters[$id]))
		{
			throw new InvalidArgumentException('filter ' . $id . ' does not exist');
		}

		return $this->filters[$id];
	}

	/**
	 * Return the list of filters.
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return $this->filters;
	}
}
<?php namespace mef\StringInterpolation;

use ArrayAccess;

/**
 * Wraps (decorates) a string interpolator by replacing the requested string
 * format with a value from an ArrayAccess object.
 *
 * This can be useful when implementing something like a resource bundle, where
 * the code references some string identifier that is replaced at run time with
 * some concrete value.
 */
class IndirectInterpolator implements StringInterpolatorInterface,
	StringInterpolatorAwareInterface
{
	use StringInterpolatorAwareTrait;

	/**
	 * @var ArrayAccess
	 */
	protected $dataSource;

	/**
	 * Constructor
	 *
	 * @param ArrayAccess $dataSource
	 * @param \mef\StringInterpolation\StringInterpolatorInterface
	 *           $stringInterpolator
	 */
	public function __construct(ArrayAccess $dataSource,
		StringInterpolatorInterface $stringInterpolator)
	{
		$this->dataSource = $dataSource;
		$this->stringInterpolator = $stringInterpolator;
	}

	/**
	 * Set the data source used to transform the input format.
	 *
	 * @param ArrayAccess $dataSource
	 */
	final public function setDataSource(ArrayAccess $dataSource)
	{
		$this->dataSource = $dataSource;
	}

	/**
	 * Get the current data source.
	 *
	 * @return ArrayAccess
	 */
	final public function getDataSource()
	{
		return $this->dataSource;
	}

	/**
	 * Given a lookup key and an array of context, return a new Interpolation
	 * record, from which the interpolated string and used context can be
	 * retrieved.
	 *
	 * The the string format will be fetched from the data source $string
	 * parameter as the key. If the key does not exist in the data source, then
	 * the key itself is used as the format.
	 *
	 * @param  string $string  The key to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return \mef\StringInterpolation\InterpolationInterface
	 */
	public function interpolate($string, $context)
	{
		if ($this->dataSource->offsetExists($string) === false)
		{
			return new Interpolation($string, []);
		}
		else
		{
			return $this->stringInterpolator->interpolate(
				$this->dataSource->offsetGet($string), $context);
		}
	}

	/**
	 * Equivalent to interpolate($string, $context)->getString()
	 *
	 * @param  string $string  The key to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return string
	 */
	public function getInterpolatedString($string, $context)
	{
		if ($this->dataSource->offsetExists($string) === false)
		{
			return $string;
		}
		else
		{
			return $this->stringInterpolator->getInterpolatedString(
				$this->dataSource->offsetGet($string), $context);
		}
	}

	/**
	 * An alias for getInterpolatedString()
	 *
	 * @param  string $string  The key to interpolate
	 * @param  array|mef\StringInterpolation\ContextInterface $context
	 *
	 * @return string
	 */
	public function __invoke($string, $context)
	{
		if ($this->dataSource->offsetExists($string) === false)
		{
			return $string;
		}
		else
		{
			return $this->stringInterpolator->getInterpolatedString(
				$this->dataSource->offsetGet($string), $context);
		}
	}
}
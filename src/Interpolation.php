<?php namespace mef\StringInterpolation;

/**
 * The result of a string interpolation.
 */
class Interpolation implements InterpolationInterface
{
	protected $string;
	protected $usedContext;

	/**
	 * Constructor
	 *
	 * @param string $string           The interpolated string
	 * @param array  $usedContext      The used context
	 */
	public function __construct($string, array $usedContext)
	{
		$this->string = (string) $string;
		$this->usedContext = $usedContext;
	}

	/**
	 * The interpolated string.
	 *
	 * @return string
	 */
	public function getString()
	{
		return $this->string;
	}

	/**
	 * The used context.
	 *
	 * @return array
	 */
	public function getUsedContext()
	{
		return $this->usedContext;
	}
}
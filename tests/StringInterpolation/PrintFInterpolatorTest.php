<?php namespace mef\Test\StringInterpolation;

class PrintfInterpolatorTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->interpolator = new \mef\StringInterpolation\PrintFInterpolator($stringifier);
	}

	public function testNoInterpolation()
	{
		$text = 'Hello, World!';
		$context = ['foo' => 'bar'];

		$this->assertSame($text, $this->interpolator->getInterpolatedString($text, $context));

		$interpolation = $this->interpolator->interpolate($text, $context);

		$this->assertSame([], $interpolation->getUsedContext());
	}

	public function testRegularInterpolation()
	{
		$interpolation = $this->interpolator->interpolate('Hello, %s!', ['World']);

		$this->assertSame('Hello, World!', $interpolation->getString());

		$this->assertSame(['World'], $interpolation->getUsedContext());
	}

	public function testInsufficientParameters()
	{
		$text = '%d %s';
		$this->assertSame(
			$this->interpolator->getInterpolatedString($text, []),
			$this->interpolator->getInterpolatedString($text, [null, null])
		);
	}

	public function testExtraParameters()
	{
		$interpolation = $this->interpolator->interpolate('%d', [0, 1]);

		$this->assertSame([0], $interpolation->getUsedContext());
	}
}

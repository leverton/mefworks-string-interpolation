<?php namespace mef\Test\StringInterpolation;

use mef\StringInterpolation\Functions;

class FunctionsTest extends \PHPUnit_Framework_TestCase
{
	public function testCastContext()
	{
		$context = Functions::castContext(['foo' => 'bar']);
		$this->assertTrue($context instanceof \mef\StringInterpolation\ContextInterface);
		$this->assertSame('bar', $context->getValue('foo'));

		$context = new \mef\StringInterpolation\ArrayContext(['foo' => 'bar']);
		$context2 = Functions::castContext($context);
		$this->assertSame($context, $context2);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidCastContext()
	{
		Functions::castContext('foo');
	}
}
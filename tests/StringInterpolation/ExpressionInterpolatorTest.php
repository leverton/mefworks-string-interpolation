<?php namespace mef\Test\StringInterpolation;

class ExpressionInterpolatorTest extends \PHPUnit_Framework_TestCase
{
	public function testFilterAccessors()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;

		// Default filters should be empty
		$this->assertEmpty($interpolator->getFilters());

		$filter = function() { };

		// Should be able to get back the same filter we set
		$interpolator->setFilter('a', $filter);
		$this->assertSame($filter, $interpolator->getFilter('a'));

		// Calling setFilters should override existing list
		$filters = ['b' => $filter];
		$interpolator->setFilters($filters);
		$this->assertSame($filters, $interpolator->getFilters());

		// But extending a filter should add to it
		$interpolator->extendFilters(['a' => $filter]);
		$this->assertSame($filter, $interpolator->getFilter('a'));
		$this->assertSame($filter, $interpolator->getFilter('b'));
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testBadSetFilters()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;
		$interpolator->setFilters(['a' => null]);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testBadExtendFilters()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;
		$interpolator->extendFilters(['a' => null]);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testBadGetFilter()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;
		$interpolator->getFilter('a');
	}

	public function testNoExpression()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;

		$string = 'Hello';
		$context = ['World'];

		// Both $string and $context should be unmodified since there are no expressions
		$this->assertSame($string, $interpolator->getInterpolatedString($string, $context));
		$this->assertSame([], $interpolator->interpolate($string, $context)->getUsedContext());
	}

	public function testPlainExpression()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;

		$string = 'Hello, {0}!';
		$context = ['World', 'Unused'];

		// $string should be interpolated with World.
		$this->assertSame('Hello, World!', $interpolator->getInterpolatedString($string, $context));
		$this->assertSame(['World'], $interpolator->interpolate($string, $context)->getUsedContext());
	}

	public function testFilteredExpression()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;
		$interpolator->setFilter('upper', 'strtoupper');
		$interpolator->setFilter('substr', 'substr');

		$string = 'Hello, {0|upper}!';
		$context = ['World', 'Unused'];

		// $string should be interpolated with uppercase variant of 'world'
		$this->assertSame('Hello, WORLD!', $interpolator->getInterpolatedString($string, $context));
		$this->assertSame(['World'], $interpolator->interpolate($string, $context)->getUsedContext());

		$string = '{name|substr:0:1}!';
		$context = ['name' => 'John Doe'];

		// $string should use the first letter of the 'name' value, followed by '!'
		$this->assertSame('J!', $interpolator->getInterpolatedString($string, $context));
	}

	public function testStringifier()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator($stringifier);

		$date = new \DateTimeImmutable('10-10-2010 10:10:10');

		$string = 'The time is {0}';
		$context = [$date];

		// With a stringifier attached, this should emit no warnings of any kind
		$this->assertTrue(is_string($interpolator->getInterpolatedString($string, $context)));
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidFilteredExpression()
	{
		$interpolator = new \mef\StringInterpolation\ExpressionInterpolator;

		$interpolator->interpolate('{0|invalid}!', []);
	}
}
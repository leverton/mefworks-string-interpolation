<?php namespace mef\Test\StringInterpolation;

use mef\Stringifier\Stringifier;
use mef\StringInterpolation\PlaceholderInterpolator;
use mef\StringInterpolation\StringInterpolatorAwareTrait;
use mef\StringInterpolation\StringInterpolatorAwareInterface;

class StringInterpolatorAwareTraitTest extends \PHPUnit_Framework_TestCase
{
	public function testStringifierAccess()
	{
		$obj = new StringInterpolatorAwareTraitSample;

		$this->assertNull($obj->getStringInterpolator());

		$interpolator = new PlaceholderInterpolator(new Stringifier);
		$obj->setStringInterpolator($interpolator);

		$this->assertSame($interpolator, $obj->getStringInterpolator());
	}
}

class StringInterpolatorAwareTraitSample implements StringInterpolatorAwareInterface
{
	use StringInterpolatorAwareTrait;
}
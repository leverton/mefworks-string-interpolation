<?php namespace mef\Test\StringInterpolation;

class PlaceholderInterpolatorTest extends \PHPUnit_Framework_TestCase
{
	public function testStringifierAccess()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$this->assertSame($stringifier, $interpolator->getStringifier());

		$stringifier = new \mef\Stringifier\Stringifier;
		$this->assertNotSame($stringifier, $interpolator->getStringifier());

		$interpolator->setStringifier($stringifier);
		$this->assertSame($stringifier, $interpolator->getStringifier());
	}

	public function testInterpolateEmptyString()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$context = ['foo' => 'bar'];

		$interpolation = $interpolator->interpolate('', $context);

		$this->assertTrue($interpolation instanceof \mef\StringInterpolation\InterpolationInterface);
		$this->assertEquals('', $interpolation->getString());
		$this->assertEquals([], $interpolation->getUsedContext());
	}

	public function testInterpolateSimpleContext()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$message = 'Foo is {foo}';
		$context = ['foo' => 'bar'];

		$interpolation = $interpolator->interpolate($message, $context);

		$this->assertEquals(
			'Foo is ' . $stringifier->stringify($context['foo']),
			$interpolation->getString()
		);

		$this->assertSame($context, $interpolation->getUsedContext());
	}

	public function testMissingContext()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$message = 'Foo is {foo}';
		$context = ['faz' => 'bar'];

		$interpolation = $interpolator->interpolate($message, $context);

		$this->assertEquals(
			'Foo is ' . $stringifier->stringify(null),
			$interpolation->getString()
		);

		$this->assertEquals(['foo' => null], $interpolation->getUsedContext());
	}

	public function testUnfinishedPlaceholder()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$message = 'Testing {placeholder';
		$context = [];

		$interpolation = $interpolator->interpolate($message, $context);

		$this->assertEquals($message, $interpolation->getString());
	}

	public function testEscaping()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$interpolation = $interpolator->interpolate('Testing \\{escape}', []);
		$this->assertEquals('Testing {escape}', $interpolation->getString());

		$interpolation = $interpolator->interpolate('Testing {escape\\}', []);
		$this->assertEquals('Testing {escape}', $interpolation->getString());

		$interpolation = $interpolator->interpolate('Testing \\', []);
		$this->assertEquals('Testing \\', $interpolation->getString());

		$interpolation = $interpolator->interpolate('Testing \\\\', []);
		$this->assertEquals('Testing \\', $interpolation->getString());

		$interpolation = $interpolator->interpolate('Testing \\n', []);
		$this->assertEquals('Testing \\n', $interpolation->getString());
	}

	public function testGetInterpolatedString()
	{
		$stringifier = new \mef\Stringifier\Stringifier;
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator($stringifier);

		$message = 'Foo is {foo}';
		$context = ['faz' => 'bar'];

		$interpolation = $interpolator->interpolate($message, $context);

		$this->assertSame(
			$interpolation->getString(),
			$interpolator($message, $context)
		);

		$this->assertSame(
			$interpolation->getString(),
			$interpolator->getInterpolatedString($message, $context)
		);
	}
}
<?php namespace mef\Test\StringInterpolation;

use ArrayObject;
use mef\StringInterpolation\IndirectInterpolator;
use mef\StringInterpolation\PrintFInterpolator;

class IndirectInterpolatorTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->data = new ArrayObject([
			'hello' => 'Hello, %s'
		]);

		$this->interpolator = new IndirectInterpolator($this->data, new PrintFInterpolator);
	}

	public function testGetDataSource()
	{
		$this->assertSame($this->data, $this->interpolator->getDataSource());
	}

	public function testSetDataSource()
	{
		$newData = new ArrayObject;
		$this->interpolator->setDataSource($newData);
		$this->assertSame($newData, $this->interpolator->getDataSource());
	}

	public function testGetInterpolatedString()
	{
		$this->assertSame('Hello, Matthew', $this->interpolator->getInterpolatedString('hello', ['Matthew']));
	}

	public function testGetMissingInterpolatedString()
	{
		// missing ids should default to using the id as the string
		$this->assertSame('missing', $this->interpolator->getInterpolatedString('missing', ['Matthew']));
	}

	public function testInterpolation()
	{
		$this->assertSame(
			$this->interpolator->getInterpolatedString('hello', ['Matthew']),
			$this->interpolator->interpolate('hello', ['Matthew'])->getString()
		);
	}

	public function testGetMissingInterpolation()
	{
		$this->assertSame(
			$this->interpolator->getInterpolatedString('missing', ['Matthew']),
			$this->interpolator->interpolate('missing', ['Matthew'])->getString()
		);
	}

	public function testInvoke()
	{
		$interpolator = $this->interpolator;

		$this->assertSame(
			$this->interpolator->getInterpolatedString('hello', ['Matthew']),
			$interpolator('hello', ['Matthew'])
		);
	}

	public function testGetMissingInvoke()
	{
		$interpolator = $this->interpolator;

		$this->assertSame(
			$this->interpolator->getInterpolatedString('missing', ['Matthew']),
			$interpolator('missing', ['Matthew'])
		);
	}
}